package ec.edu.ups.appdis.VIEW;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ec.edu.ups.appdis.EN.Persona;
import ec.edu.ups.appdis.ON.ContactosLocal;
@WebServlet("/local")
public class ClienteWebLocal extends HttpServlet {

	@Inject
	private ContactosLocal local;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {	
		Persona p = new Persona();
		p.setCedula("0303013072");
		p.setNombre("julio");
		p.setApellido("chuqui");
		try {
			local.guardarContacto(p);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		List<Persona> personas = local.listarContactos();
		for(Persona p1 : personas) {
			response.getWriter().println(p1.getNombre());
			System.out.print(p1);
		}
}
}
