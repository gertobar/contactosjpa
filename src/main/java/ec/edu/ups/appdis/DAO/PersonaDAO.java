package ec.edu.ups.appdis.DAO;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.appdis.EN.Persona;

@Stateless
public class PersonaDAO {

	@PersistenceContext 
	private EntityManager em;

	public void insert(Persona persona) {
		em.persist(persona);
	}
	public void update(Persona persona) {
		em.merge(persona);
	}

	public void remove(String cedula) {
		em.remove(this.searchPersona(cedula));
	}

	public Persona searchPersona(String cedula) {
		Persona persona = em.find(Persona.class, cedula);
		return persona;
	}
	
	public List<Persona> getlistPersonaTelefonos(){
		String jpql = "SELECT p FROM Persona p";
		Query query = em.createQuery(jpql, Persona.class);
		List<Persona> listado =  query.getResultList();	
		return listado;
	}
	

   
}
