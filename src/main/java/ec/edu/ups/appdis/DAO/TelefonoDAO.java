package ec.edu.ups.appdis.DAO;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.appdis.EN.Persona;
import ec.edu.ups.appdis.EN.Telefono;

@Stateless
public class TelefonoDAO {

	@PersistenceContext
	private EntityManager em;

	public void insert(Telefono telefono) {
		em.persist(telefono);
	}
	public ArrayList<Telefono> listarTelefonos(){
		String jpql = "SELECT t FROM TelefonoEN t";
		Query query = em.createQuery(jpql, Telefono.class);
		ArrayList<Telefono> listado = (ArrayList<Telefono>) query.getResultList();	
		return listado;
	}
	
	public void removeContactos(String cedula) {
		String sql = "DELETE FROM Telefono WHERE personaCedula = :cedula";
		Query query = em.createNativeQuery(sql);
		query.setParameter("cedula", cedula);
		query.executeUpdate();
	}
	
	public Telefono searchTelefonos(String cedula) {
		Telefono telefono = em.find(Telefono.class, cedula);
		return telefono;
	}

}

