package ec.edu.ups.appdis.BEAN;


import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import ec.edu.ups.appdis.EN.Persona;
import ec.edu.ups.appdis.EN.Telefono;
import ec.edu.ups.appdis.ON.ContactosLocal;

//@ManagedBean
//@ViewScoped
@Named
@ConversationScoped
public class ContactosBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private ContactosLocal contlocal;
	
	private Persona persona;
	
	private List<Persona> personas;
	
	private String cedula;
	private String titulo;
	
	@PostConstruct
	public void init() {
		persona = new Persona();
		persona.agregarTelefono(new Telefono());
		loadDataContactos();
	}
	
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
		if(cedula != null) {
			persona = contlocal.getPersona(cedula);	
		}
	}
	


	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public List<Persona> getPersonas() {
		return personas;
	}
	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}
	
	public String guardarDatos() {

		try {
			contlocal.guardarContacto(persona);
			init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(this.toString());
		return "listar";
		
	}
	public void loadDataContactos() {
		personas = contlocal.listarContactos();
	}
	public String agregarTelefono() {
		persona.agregarTelefono(new Telefono());
		return null;
	}
	
	public String editar(String cedula) {
		System.out.println(cedula);
		return "crear?faces-redirect=true&cedula="+cedula;
	}
	
	public String eliminar(String cedula) {
		System.out.println(cedula);
		contlocal.EliminarPersona(cedula);
		loadDataContactos();
		return null;
	}
	public String noEliminar(String cedula) {
		loadDataContactos();
		return null;
	}
	
	public void confirm(String cedula) {  
		addMessage("Contacto",cedula+" Eliminado");  
		System.out.println(cedula);
		contlocal.EliminarPersona(cedula);
		loadDataContactos();
		}  
	public void addMessage(String summary, String cedula) {  
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, cedula);  
		FacesContext.getCurrentInstance().addMessage(null, message);  
		}  
	
}
