package ec.edu.ups.appdis.ON;

import java.util.List;

import javax.ejb.Remote;

import ec.edu.ups.appdis.EN.Persona;

@Remote
public interface ContactosRemote {

	public void guardarContacto(Persona persona)throws Exception;

	public List<Persona> listarContactos();

	public Persona getPersona(String cedula);
	
	public Persona searchPersona(String cedula);

	public boolean validadorDeCedula(String cedula) throws Exception;
	
	public void EliminarPersona(String cedula);

}
