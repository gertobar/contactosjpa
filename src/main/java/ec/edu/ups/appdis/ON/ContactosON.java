package ec.edu.ups.appdis.ON;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.DAO.PersonaDAO;
import ec.edu.ups.appdis.DAO.TelefonoDAO;
import ec.edu.ups.appdis.EN.Persona;

@Stateless
public class ContactosON implements ContactosRemote, ContactosLocal {

	@Inject
	private PersonaDAO personaDAO;

	@Inject
	private TelefonoDAO telefonoDAO;

	@Override
	public void guardarContacto(Persona persona) throws Exception {
		if (validadorDeCedula(persona.getCedula())) {
			persona.getTelefonos();
			Persona aux = personaDAO.searchPersona(persona.getCedula());
			if(aux != null) {
				
				personaDAO.update(persona);
			}else {
			persona.getTelefonos();
			personaDAO.insert(persona);
			}

			
		} else {
			throw new Exception("Cedula Incorrecta");
		}
		
	}

	@Override
	public List<Persona> listarContactos() {
		return personaDAO.getlistPersonaTelefonos();
	}

	@Override
	public Persona getPersona(String cedula) { 
        return personaDAO.searchPersona(cedula);  
    }

	@Override
	public Persona searchPersona(String cedula) {

		Persona per = personaDAO.searchPersona(cedula);
		return per;
	}
	
	@Override
	public void EliminarPersona(String cedula) {
		personaDAO.remove(cedula);
		telefonoDAO.removeContactos(cedula);
	}
	

	@Override
	public boolean validadorDeCedula(String cedula) throws Exception {
		boolean cedulaCorrecta = false;

		try {

			if (cedula.length() == 10) // ConstantesApp.LongitudCedula
			{
				int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
				if (tercerDigito < 6) {
// Coeficientes de validación cédula
// El decimo digito se lo considera dígito verificador
					int[] coefValCedula = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
					int verificador = Integer.parseInt(cedula.substring(9, 10));
					int suma = 0;
					int digito = 0;
					for (int i = 0; i < (cedula.length() - 1); i++) {
						digito = Integer.parseInt(cedula.substring(i, i + 1)) * coefValCedula[i];
						suma += ((digito % 10) + (digito / 10));
					}

					if ((suma % 10 == 0) && (suma % 10 == verificador)) {
						cedulaCorrecta = true;
					} else if ((10 - (suma % 10)) == verificador) {
						cedulaCorrecta = true;
					} else {
						cedulaCorrecta = false;
					}
				} else {
					cedulaCorrecta = false;
				}
			} else {
				cedulaCorrecta = false;
			}
		} catch (NumberFormatException nfe) {
			cedulaCorrecta = false;
		} catch (Exception err) {
			cedulaCorrecta = false;
			throw new Exception("Una excepcion ocurrio en el proceso de validadcion");
		}

		if (!cedulaCorrecta) {
			throw new Exception("La Cédula ingresada es Incorrecta");
		}
		return cedulaCorrecta;
	}

}
