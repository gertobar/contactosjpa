package ec.edu.ups.appdis.EN;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Telefono implements Serializable {
	private static final long serialVersionUID = -558553967080513790L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String numero;
	private String Tipo;
	private String personaCedula;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String Numero) {
		this.numero = Numero;
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String Tipo) {
		this.Tipo = Tipo;
	}

	public String getPersonaCedula() {
		return personaCedula;
	}

	public void setPersonaCedula(String personaCedula) {
		this.personaCedula = personaCedula;
	}

}

