package ec.edu.ups.appdis.EN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Persona implements Serializable {
	
	private static final long serialVersionUID = -558553967080513790L;
    @Id
    private String cedula;
    private String nombre;
    private String apellido;
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "personaCedula")
    private List<Telefono> telefonos;
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public List<Telefono> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(List<Telefono> telefonos) {
        this.telefonos = telefonos;
    }

    public void agregarPersona(Telefono telefono) {
        
    }
    public void agregarTelefono(Telefono telefono) {
		if(this.telefonos==null)
			this.telefonos = new ArrayList<>();
		this.telefonos.add(telefono);
	}

    
   
}
